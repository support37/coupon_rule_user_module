<?php
/**
 * Created by PhpStorm.
 * User: quantt
 * Date: 1/31/2018
 * Time: 11:35 PM
 */

class Mgroup_ReactiveCoupon_Block_Adminhtml_Promo_Quote_Edit_Tab_Custom extends Mage_Adminhtml_Block_Widget_Grid
    implements Mage_Adminhtml_Block_Widget_Tab_Interface

{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sales_coupon_reactive');
        $this->setUseAjax(true);
    }

    /**
     * Mandatory to override as we are implementing Widget Tab interface
     * Return Tab Title
     *
     * @return string
     */
    public function getTabTitle(){
        return Mage::helper('salesrule')->__('Sales coupon');
    }
    /**
     * Mandatory to override as we are implementing Widget Tab interface
     * Return Tab Label
     *
     * @return string
     */
    public function getTabLabel(){
        return Mage::helper('salesrule')->__('Sales coupon');
    }
    /**
     * Mandatory to override as we are implementing Widget Tab interface
     * Can show tab in tabs
     * Here you can write condition when the tab should we shown or not. Like you see when we create shopping cart rule
     * Manage coupon tab doesn't come. If you want that then just make a function and check whether you have information
     * in registry or not
     *
     * @return boolean
     */
    public function canShowTab(){
        return true;
    }
    /**
     * Mandatory to override as we are implementing Widget Tab interface
     * Tab is Hidden
     *
     * @return boolean
     */
    public function isHidden(){
        return false;
    }
    /**
     * Defines after which tab this tab should come like you asked you need it below Manage Coupon codes
     *
     * @return string
     */
    public function getAfter(){
        return 'coupons_section';
    }
    protected function _prepareCollection()
    {
        /*Prepare collection that you want to show on the grid. I have commented it because I don't have
        any table or collection. You will be uncommenting it to set collection on your grid.
        */
        $collection = Mage::getModel('salesrule/rule_customer')->getCollection();
        $collection->addFieldToSelect('*')->getSelect()->join(
            array('c_e'=>"customer_entity"),
            'c_e.entity_id=main_table.customer_id',
            array('*')
        )
            ->group('main_table.customer_id')
            ->where('main_table.rule_id='.$this->getRequest()->getParam('id'))
        ;


        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    /**
     * This method will create columns
     */
    protected function _prepareColumns()
    {
        /*Code for your grid here.*/
        $this->addColumn('rule_customer_id', array(
            'header' => Mage::helper('salesrule')->__('ID'),
            'index'  => 'rule_customer_id',
            'type'   => 'number',
            'align'  => 'center',
        ));

        $this->addColumn('customer_id', array(
            'header' => Mage::helper('salesrule')->__('Customer Id'),
            'index'  => 'customer_id',
            'type'   => 'number',
            'align'  => 'center',
        ));

        $this->addColumn('email', array(
            'header' => Mage::helper('salesrule')->__('Email'),
            'index'  => 'email',
            'type'   => 'text',
            'align'  => 'center',
        ));

        $this->addColumn('times_used', array(
            'header' => Mage::helper('salesrule')->__('Time Used'),
            'index'  => 'times_used',
            'type'   => 'number',
            'align'  => 'center',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('email');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseAjax(false);
        $this->getMassactionBlock()->setHideFormElement(true);

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('adminhtml')->__('Delete'),
            'url'  => $this->getUrl('*/*/couponsUsedMassDelete', array('_current' => true)),
            'confirm' => Mage::helper('salesrule')->__('Are you sure you want to reactive rule for this customer?'),
        ));

        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/edit', array('id'=> $this->getId()));
    }

}
